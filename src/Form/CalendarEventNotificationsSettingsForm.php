<?php

namespace Drupal\calendar_event_notifications\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring event notification settings.
 */
class CalendarEventNotificationsSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {
  /**
   * {@inheritdoc}
   */
  protected $moduleHandler;

  /**
   * Constructs a new CalendarEventNotificationsSettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'calendar_event_notifications_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['calendar_event_notifications.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    // Load configuration.
    $config = $this->config('calendar_event_notifications.settings');

    $form['days_before_event'] = [
      '#type' => 'select',
      '#title' => $this->t('Days Before Event'),
      '#default_value' => $config->get('days_before_event'),
      '#options' => [
        1 => $this->t('1 day'),
        2 => $this->t('2 day'),
        3 => $this->t('3 day'),
        4 => $this->t('4 day'),
        5 => $this->t('5 day'),
        6 => $this->t('6 day'),
        7 => $this->t('7 day'),
        8 => $this->t('8 day'),
        9 => $this->t('9 day'),
        10 => $this->t('10 day'),
      ],
    ];

    $form['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Subject'),
    // Set a default value.
      '#default_value' => $config->get('email_subject') ?: $this->t('Remainder for your upcoming event'),
    ];

    $form['email_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email Body'),
       // Set a default value.
      '#default_value' => $config->get('email_body') ?: $this->t('Hello,
      You are invited for the event happening on the following date!
      Please check more about the event below
      Thanks & Regards'),
    ];

    // Token support.
    if ($this->moduleHandler->moduleExists('token')) {
      $form['tokens'] = [
        '#title' => $this->t('Tokens'),
        '#type' => 'container',
      ];
      $form['tokens']['help'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [
          'node',
        ],
        '#global_types' => TRUE,
        '#dialog' => TRUE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configuration.
    $config = $this->config('calendar_event_notifications.settings');
    $config->set('days_before_event', $form_state->getValue('days_before_event'));
    $config->set('email_subject', $form_state->getValue('email_subject'));
    $config->set('email_body', $form_state->getValue('email_body'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}

# Calendar event notifications

The Calendar Event Notifications module allows users to add, edit, or delete events from the calendar notification page itself. This module gives the events in the calendar form and its own content type and views. It manages the colours of events based on their status (past,present,future).

This module gives feature to send reminder emails for an upcoming event before x days of the event based on the admin configuration.

## Table of Contents

- Requirements
- Installation
- Configuration
- Maintainers
- Links


## Requirements

This module requires the following modules:

- [Fullcalendar View](https://www.drupal.org/project/fullcalendar_view)
- [Token](https://www.drupal.org/project/token)
- [DateTime Range](https://www.drupal.org/project/datetime_range) module (included in Drupal core modules)

## Installation

Install as you would normally install a contributed Drupal module. 

## Configuration

There is no configuration, No need to add content type and import views template.

## Maintainers

* Muskan Mahendra (Muskan.m) - https://www.drupal.org/u/MuskanM

## Links

https://www.drupal.org/project/calendar_event_notifications







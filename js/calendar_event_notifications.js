(function ($, Drupal) {
  Drupal.behaviors.calendarEventNotifications = {
    attach: function (context, settings) {
      if (settings.calendar_event_notifications && settings.calendar_event_notifications.event_create == true) {
        $('.fc-day-top', context).click((e) => {
          e.preventDefault();
          var clickedElement = $(e.currentTarget); // Using e.currentTarget to get the clicked element
          var dateValue = clickedElement.data('date');

          if (typeof dateValue !== 'undefined') {
              $.magnificPopup.open({
              items: {
                src: './node/add/calendar_event_notification',
              },
              type: 'ajax',
              midClick: false,
              closeOnContentClick: false,
              closeOnBgClick: false,
              enableEscapeKey: false,
              callbacks: {
                ajaxContentAdded: () => {
                  // This callback is triggered when the content is added to the DOM
                  $('#edit-field-event-date-0-value-date', context).val(dateValue);
                  const value = $('#edit-field-event-date-0-value-date', context).val();
                  const setvalue = setValResult(dateValue);
                },
              },
            });
          } else {
            console.error('Date Value is undefined or not found.');
          }

          return false;
        });

      } else {
        const buttonhide = document.querySelectorAll('.btn-views');
      }


      var fcWeekElements = context.querySelectorAll('.fc-week');
      fcWeekElements.forEach(function (fcWeekElement, index) {
        // Find the corresponding thead td with class .fc-past
        var theadTd = fcWeekElement.querySelectorAll('.fc-content-skeleton thead tr:first-child td');
        const tbodyTd = fcWeekElement.querySelectorAll('.fc-content-skeleton tbody tr:first-child td');
        const greedTd = fcWeekElement.querySelectorAll('.fc-bg tbody td');
        let eventColor = [
          { "1": 0 },
          { "2": 0 },
          { "3": 0 },
          { "4": 0 },
          { "5": 0 },
          { "6": 0 },
          { "7": 0 },
        ];
        var indexHandler = 0;

        tbodyTd.forEach(function (tbodyTd, tbodyIndex) {
          if (tbodyTd.hasAttribute("colspan")) {
            var colspan = Number(tbodyTd.getAttribute("colspan"));
            for (let i = 0; i <= 5; i++) {}
            for (let i = 0; i < colspan; i++) {
              indexHandler++;
              eventColor[indexHandler - 1][indexHandler] = 1;
            }
          } else {
            indexHandler++;
            if (tbodyTd.textContent.trim() !== '') {
              eventColor[indexHandler - 1][indexHandler] = 1;
            }
          }
        });

        theadTd.forEach(function (theadTd, theadIndex) {
          if (theadTd.classList.contains('fc-past') && eventColor[theadIndex][theadIndex + 1] == 1) {
            greedTd[theadIndex].style.backgroundColor = '#eb9a9a66';
          } else if (theadTd.classList.contains('fc-future') && eventColor[theadIndex][theadIndex + 1] == 1) {
            greedTd[theadIndex].style.backgroundColor = '#d3f5d380';
          }
        });
      });
    },
  };
})(jQuery, Drupal);

function setValResult(dateValue) {
  const newone = document.getElementById('edit-field-event-date-0-value-date');
  newone.defaultValue = dateValue;
  return "yes";
}
